const JsonData = {"lang":1, "equip":2, "items":3, "techs":4}

const TABLE_COLS = 12;
const FIELD_WIDTH = 40;
const SECONDS_PER_DAY = 20;
const ICON_FOLDER = "gamedb/icons/"

var data = {};
var object_data = {};
var item_data = {};
var tech_data = {};
var loca_data = {};

//wait until any other javascript is done and the complete site is loaded
//after that start with the replacements
$(window).on('load', function() {
    //getting the language identifier
    var pathname = window.location.pathname;
    var lang = "en";

    //define the paths to all json data files
    var paths = [];
    paths[JsonData.lang] = "gamedb/localization/goodcompanyBase_"+lang+".json";
    paths[JsonData.equip] = "gamedb/gamedata/equipment.json";
    paths[JsonData.items] = "gamedb/gamedata/materials.json";
    paths[JsonData.techs] = "gamedb/gamedata/techs.json";

    //start loading the data
    data = [];
    load_json_data(1, paths);
});


//loading data is asynchronous, we need to call the next load from within the function itself
//for that we have an index iterating throught he complete path array, write it to data and carry over the data to the next iteration
function load_json_data (index, paths) {
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            data[index] = JSON.parse(this.responseText);
            if (index < Object.keys(JsonData).length) {
                index += 1;
                load_json_data(index, paths, data)
            } else {
                execute_replacements(data)
            }
        }
    }
    xmlhttp.open("GET", paths[index], true);
    xmlhttp.send();
}


//starts the actualy replacements, each data gets iterated over twice
//first all normal data, then the classes with a 2 added. Those are normally added by the replacement script
//last action is to localize all market elements
function execute_replacements() {
    data[JsonData.equip].equipment.forEach((e) => {
        object_data[e.loca_string] = e;
    });

    data[JsonData.items].materials.forEach((e) => {
        item_data[e.loca_string] = e;
    });

    data[JsonData.techs].techs.forEach((e) => {
        tech_data[e.loca_string] = e;
    });

    loca_data = data[JsonData.lang];

    replace_data(object_data, "objectdata");
    replace_data(item_data, "itemdata");
    replace_data(tech_data, "techdata");

    replace_info(object_data, "object");
    replace_info(item_data, "item");
    replace_info(tech_data, "tech");

    localize();
    icon_replacement(default_class="gc_icon");
    page_linking();
    clean_up();
}


function clean_up() {
    var contents = document.getElementsByClassName("mw-parser-output");
    [].forEach.call(contents, function (content) {
        content.innerHTML = content.innerHTML.replace(new RegExp(escapeRegExp('[item:]'), 'g'), '',);
        content.innerHTML = content.innerHTML.replace(new RegExp(escapeRegExp('[object:]'), 'g'), '',);
        //content.innerHTML = content.innerHTML.replace(new RegExp('<tr>(([[:blank:]]|\n)*<td>([[:blank:]]|\n)*<\/td>([[:blank:]]|\n)*)+<\/tr>'), 'g');
    });
}


function page_linking() {
    var contents = document.getElementsByClassName("mw-parser-output");
    [].forEach.call(contents, function (content) {
        var results = content.innerHTML.match(/(\[l:)([\w]*):*([\w]+)*(])([\w ]+)(\[\/l\])/g);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var mainresult = results[i].match(/(\[l:)([\w]*):*([\w]+)*(])([\w ]+)(\[\/l\])/);
                var s = `<a href="https://wiki.goodcompanygame.com/index.php/${mainresult[2]}">${mainresult[5]}</a>`;
                content.innerHTML = content.innerHTML.replace(new RegExp(escapeRegExp(mainresult[0])), s);
            }
        }
    });
}


function localize() {
    var contents = document.getElementsByClassName("mw-parser-output");
    [].forEach.call(contents, function (content) {
        // replace loca_string replacements e.g. [t:itm_led_matrix]
        var results = content.innerHTML.match(/(\[t:)([\w]*):*([\w]+)*(])/g);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var mainresult = results[i].match(/(\[t:)([\w]*):*([\w]+)*(])/)

                var s = loca_data[mainresult[2]];
                var param = mainresult[3]

                if (mainresult[2] == null || s == null) {
                    content.innerHTML = content.innerHTML.replace('[t:]', '');
                    continue;
                }

                if (param == "only_portrait") {
                    var result = s.match(/(\[portrait:)([\w/]+)(])/);
                    if (result != null) {
                        var image_path = (result[2])+'.png';
                        s = `<img class="gc_portrait" src="${ICON_FOLDER}${image_path}">`;
                    }
                }
                
                if (param == "only_name") {
                    var result = s.match(/(\[n:)([\w]+)(])/);
                    if (result != null) {
                        s = loca_data[result[2]]
                    }
                }
                
                if (param == "remove_portrait" || param == "only_text") {
                    var result = s.match(/(\[portrait:)([\w/]+)(])/);
                    if (result != null) {
                        s = s.replace(result[0], "");
                    }
                } 
                
                if (param == "remove_name" || param == "only_text") {
                    var result = s.match(/(\[n:)([\w]+)(])/);
                    if (result != null) {
                        s = s.replace(result[0], "");
                    }
                }

                var result = s.match(/(\[portrait:)([\w/]+)(])/);
                if (result != null) {
                    var image_path = (result[2])+'.png';
                    s = s.replace(result[0], `<img class="gc_icon" src="${ICON_FOLDER}${image_path}"> `);
                }

                var result = s.match(/(\[n:)([\w]+)(])/);
                if (result != null) {
                    s = s.replace(result[0], loca_data[result[2]]+": " );
                }

                content.innerHTML = content.innerHTML.replace(mainresult[0], s.replace(/\n/g, "<br />"));
            }
        }
        
        var results = content.innerHTML.match(/(\[t:)([\w]+)(])/g);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var result = results[i].match(/(\[t:)([\w]+)(])/)
                content.innerHTML = content.innerHTML.replace(result[0], loca_data[result[2]].replace(/\n/g, "<br />"));
            }
        }

        var results = content.innerHTML.match(/(\[highlight:)([\w]+)(])/g);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var result = results[i].match(/(\[highlight:)([\w]+)(])/)
                content.innerHTML = content.innerHTML.replace(result[0], "");
            }
        }

        var results = content.innerHTML.match(/(\[n:)([\w]+)(])/g);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var result = results[i].match(/(\[n:)([\w]+)(])/)
                content.innerHTML = content.innerHTML.replace(result[0], "");
            }
        }

        var results = content.innerHTML.match(/(\[portrait:)([\w/]+)(])/g);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var result = results[i].match(/(\[portrait:)([\w/]+)(])/)
                content.innerHTML = content.innerHTML.replace(result[0], "");
            }
        }

        // replace **
        var results = content.innerHTML.match(/(\*\*)([\w\s]+)(\*\*)/g);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var result = results[i].match(/(\*\*)([\w\s]+)(\*\*)/)
                s = s.replace(result[0], `<span style="font-style: italic!important;">${result[2]}</span>`);
            }
        }

        // replace *
        var results = content.innerHTML.match(/(\*)([\w\s]+)(\*)/g);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var result = results[i].match(/(\*)([\w\s]+)(\*)/)
                content.innerHTML = content.innerHTML.replace(result[0], `<span style="font-weight: 800!important;">${result[2]}</span>`);
            }
        }

    });
}


function icon_replacement(default_class="gc_icon") {
    var contents = document.getElementsByClassName("mw-parser-output");
    [].forEach.call(contents, function (content) {

        var regex_string = `\\[s:([\\w\\/-]+):*([\\w+-:=\\s]+)*\\]`;
        var global_regex = new RegExp(regex_string, 'g');
        var local_regex = new RegExp(regex_string);

        var results = content.innerHTML.match(global_regex);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var result = results[i].match(local_regex)
                if (result[1] == "/") {
                    content.innerHTML = content.innerHTML.replace('[s:/]', '');
                    continue;
                }
                var image_path = ICON_FOLDER+(result[1])+'.png';
                var filter = ``;
                var img_class = default_class
                var tooltip = ""

                var params = "" 
                if (result[2] != null) {
                    params = result[2].split(":");
                }
                
                for (var param_index=0; param_index < params.length; param_index++) {

                    switch (params[param_index]) {
                        case "t":
                            filter = `style="filter: brightness(0.25);"`;
                            break;
                        case "smaller": case "--": case "xs":
                            img_class = "gc_icon_smaller"
                            break;
                        case "small": case "-": case "s":
                            img_class = "gc_icon_small"
                            break;
                        case "medium": case "m":
                            img_class = "gc_icon"
                            break;
                        case "big": case "+": case "l":
                            img_class = "gc_icon_big"
                            break;
                        case "bigger": case "++": case "xl":
                            img_class = "gc_icon_bigger"
                            break;
                        case "giant": case "+++": case "xxl":
                            img_class = "gc_icon_giant"
                            break;
                        case "portrait_small": case "ps":
                            img_class = "gc_portrait_small"
                            break;
                        case "portrait": case "pn":
                            img_class = "gc_portrait"
                            break;
                        case "portrait_large": case "pl":
                            img_class = "gc_portrait_large"
                            break;
                        case "portrait_largeer": case "pxl":
                            img_class = "gc_portrait_larger"
                            break;
                        default:
                            if (params[param_index].substring(0,7) == "tooltip") {
                                tooltip = "<span class='gc_tooltip'>"+params[param_index].substring(8)+"</span>";
                            }
                    } 
                }
                content.innerHTML = content.innerHTML.replace(result[0], `<img class="${img_class}" ${filter} src="${image_path}">${tooltip}`);
            }
        }
    });
}



function replace_info(data_table, identifier) {
    var contents = document.getElementsByClassName("mw-parser-output");
    [].forEach.call(contents, function (content) {
        var regex_string = `\\[${identifier}:+([\\w]+):*([\\w:=]+)*\\]`;
        var global_regex = new RegExp(regex_string, 'g');
        var local_regex = new RegExp(regex_string);
        
        var results = content.innerHTML.match(global_regex);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var result = results[i].match(local_regex);

                if (result[1] == null) {
                    content.innerHTML = content.innerHTML.replace(new RegExp(escapeRegExp(result[0]), 'g'), ``);
                    continue;
                }

                var data = data_table[result[1]];
                if (data == null) {
                    content.innerHTML = content.innerHTML.replace(new RegExp(escapeRegExp(result[0]), 'g'), ``);
                    continue;
                }

                var params = "" 
                if (result[2] != null)
                    params = result[2].split(":");
                

                var s = ""
                var type = "full"
                var nolink = false
                var notip = false
                var icon_class = ""

                for (var param_index=0; param_index < params.length; param_index++) {
                    if (params[param_index].substring(0,4) == "type") {
                        type = params[param_index].substring(5);
                    } else if (params[param_index] == "nolink") {
                        nolink = true;
                    } else if (params[param_index] == "notip") {
                        notip = true;
                    } else if (params[param_index].substring(0,10) == "icon_class") {
                        icon_class = params[param_index].substring(11);
                    }
                }

                switch (type) {
                    case "text":
                        s = loca_data[data.loca_string];
                        break;
                    case "icon":
                        if (icon_class=="")
                            icon_class = "gc_icon";

                        s = `<img class="${icon_class}"
                            src="${ICON_FOLDER}${data.icon_sprite}/${data.icon_id}.png" />`;

                        if (!notip)
                            s += `<span class="gc_tooltip">${loca_data[data.loca_string]}</span>`;
                        break;
                    default:
                        if (icon_class=="")
                            icon_class = "gc_icon";

                        s = `<img class="${icon_class}"
                            src="${ICON_FOLDER}${data.icon_sprite}/${data.icon_id}.png" />
                            ${loca_data[data.loca_string]}`;
                        break;
                }

                if (!nolink)
                    s = `<a href="index.php?title=${loca_data[data.loca_string]}_(${identifier})">${s}</a>`;
                s = `<span class="item_info">${s}</span>`;

                content.innerHTML = content.innerHTML.replace(new RegExp(escapeRegExp(result[0]), 'g'), s);
            }
        }
    });
}


function replace_data(data_table, identifier) {
    var contents = document.getElementsByClassName("mw-parser-output");
    [].forEach.call(contents, function (content) {

        var regex_string = `\\[${identifier}:+([\\w]+):*([\\w:]+)*\\]`;
        var global_regex = new RegExp(regex_string, 'g');
        var local_regex = new RegExp(regex_string);

        var results = content.innerHTML.match(global_regex);
        if (results != null) {
            for (var i=0; i < results.length; i++) {
                var result = results[i].match(local_regex);

                if (result[1] == null || result[2] == null) {
                    content.innerHTML = content.innerHTML.replace(new RegExp(escapeRegExp(result[0]), 'g'), ``);
                    continue;
                }

                var data = data_table[result[1]];
                if (data == null) {
                    content.innerHTML = content.innerHTML.replace(new RegExp(escapeRegExp(result[0]), 'g'), ``);
                    continue;
                }

                var params = result[2].split(":");
                var s = data[params[0]];
                
                for(var j = 1; j<params.length; j++) {
                    if (s != null || typeof s !== "undefined") {
                        s = s[params[j]];
                    }
                }

                if (typeof s === "undefined") {
                    s = "";
                } else if (typeof s !== "string" && typeof s !== "number" && typeof s !== "boolean") {
                    s = true;
                }

                content.innerHTML = content.innerHTML.replace(new RegExp(escapeRegExp(result[0]), 'g'), s);
            }
        }
    });
}

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}